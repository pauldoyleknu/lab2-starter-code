#!/usr/bin/python
import sys

# Author: Paul Doyle

# find out how many arguments were on the command line
# use the lenght of the argv variable which contains a list of the arguments
num_args = len(sys.argv)
print 'Number of arguments given was :', num_args, 'arguments.'

# Now we can loop through the 
for i in range (num_args):
	print 'Argument ',i,' is ', str(sys.argv[i])
